from snake import Game
from sys import stderr

print('Welcome to snake!\nwasd or arrows -> snake direction\np -> pause\nr -> restart\n'
      'argv[1] -> movement interval, s, float', file=stderr)

Game().run()
