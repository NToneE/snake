import argparse
import enum
import pathlib
import pygame
import random
from time import perf_counter
from typing import Optional

parser = argparse.ArgumentParser(prog='snake', description='Pygame snake by ntonee')
parser.add_argument('-f', '--field', help='width,height', type=lambda s: (int(s.split(',')[0]), int(s.split(',', maxsplit=1)[1])), default=(16, 10))
parser.add_argument('-d', '--delay', help='movement delay', type=float, default=0.2)
parser.add_argument('-w', '--walls', help='disallow snake to encounter borders', action='store_true')

args = parser.parse_args()

ASSETS_DIR = pathlib.Path('assets')

CELL_SIZE = (64, 64)
FIELD_SIZE = args.field
SCREEN_SIZE = (CELL_SIZE[0] * FIELD_SIZE[0], CELL_SIZE[1] * FIELD_SIZE[1])

MOVE_DELAY_SECONDS = args.delay

NO_WALL_MODE = not args.walls

class Asset:
    filename: str
    surface: pygame.Surface
    rect: pygame.Rect

    def __init__(self, filename):
        self.filename = filename

    def load(self):
        self.surface = pygame.transform.scale(pygame.image.load(ASSETS_DIR / self.filename).convert_alpha(), CELL_SIZE)
        self.rect = self.surface.get_rect()


class Assets(Asset, enum.Enum):
    SNAKE_BODY = 'snake_body.png'
    SNAKE_HEAD_LEFT = 'snake_head_left.png'
    APPLE = 'apple.png'
    BACKGROUND_1 = 'gray.png'
    BACKGROUND_2 = 'black.png'


class Direction(int, enum.Enum):
    RIGHT = 0
    UP = 1
    LEFT = 2
    DOWN = 3


class Snake:
    positions: list[tuple[int, int]]
    last_step_direction: Direction
    direction: Direction
    last_move: int

    def __init__(self):
        self.positions = [(2, 0), (1, 0), (0, 0)]
        self.direction = self.last_step_direction = Direction.RIGHT
        self.last_move = int(perf_counter() // MOVE_DELAY_SECONDS)

    def move(self):
        current_time = int(perf_counter() // MOVE_DELAY_SECONDS)
        if self.last_move == current_time:
            return None
        self.last_step_direction = self.direction
        self.last_move = current_time
        # returns where the tail was
        result = self.positions[-1]
        self.positions = [self.positions[0]] + self.positions[:-1]
        if self.direction == Direction.RIGHT:
            self.positions[0] = (self.positions[0][0] + 1, self.positions[0][1])
        elif self.direction == Direction.UP:
            self.positions[0] = (self.positions[0][0], self.positions[0][1] - 1)
        elif self.direction == Direction.LEFT:
            self.positions[0] = (self.positions[0][0] - 1, self.positions[0][1])
        elif self.direction == Direction.DOWN:
            self.positions[0] = (self.positions[0][0], self.positions[0][1] + 1)
        if NO_WALL_MODE:
            self.positions[0] = tuple(self.positions[0][i] % FIELD_SIZE[i] for i in (0, 1))
        return result


class Game:
    screen: pygame.Surface
    background: pygame.Surface
    clock: pygame.time.Clock
    snake: Snake
    apple_pos: Optional[tuple[int, int]]
    running: bool
    paused: bool

    def __init__(self):
        self.running = True
        self.paused = False
        pygame.init()
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        # self.background = pygame.Surface(self.screen.get_size())
        pygame.display.set_caption('snake', 'snake')
        # self.background.fill((100, 100, 100))
        self.clock = pygame.time.Clock()
        self.snake = Snake()
        for asset in Assets.__members__.values():
            asset.load()
        self.set_apple_pos()
        pygame.display.flip()

    def set_apple_pos(self):
        free_tiles = [(i, j) for i in range(FIELD_SIZE[0]) for j in range(FIELD_SIZE[1])
                      if (i, j) not in self.snake.positions]
        if not free_tiles:
            self.apple_pos = None
        else:
            self.apple_pos = random.choice(free_tiles)

    def tick(self):
        if self.running and (tail := self.snake.move()):
            win = False
            if self.snake.positions[0] == self.apple_pos:
                self.snake.positions.append(tail)
                print(f'Score: {len(self.snake.positions)}')
                self.set_apple_pos()
                if len(self.snake.positions) == FIELD_SIZE[0] * FIELD_SIZE[1]:
                    self.running = False
                    win = True
            if self.snake.positions[0] in self.snake.positions[1:]:
                self.running = False
            if not (0 <= self.snake.positions[0][0] < FIELD_SIZE[0] and 0 <= self.snake.positions[0][1] < FIELD_SIZE[1]):
                self.running = False
            if not self.running and not win:
                self.snake.positions.append(tail)
                self.snake.positions = self.snake.positions[1:]
        for i in range(FIELD_SIZE[0]):
            for j in range(FIELD_SIZE[1]):
                self.screen.blit(Assets.BACKGROUND_2.surface if (i % 2 != j % 2) else Assets.BACKGROUND_1.surface,
                                 pygame.Rect((i * CELL_SIZE[0], j * CELL_SIZE[1]), CELL_SIZE))
        for snakepos in self.snake.positions[1:]:
            self.screen.blit(Assets.SNAKE_BODY.surface, pygame.Rect((snakepos[0] * CELL_SIZE[0], snakepos[1] * CELL_SIZE[1]),
                                                                    CELL_SIZE))
        head = Assets.SNAKE_HEAD_LEFT.surface
        head = pygame.transform.rotate(head, 90 * ((self.snake.direction.value - Direction.LEFT.value) % 4))
        self.screen.blit(head, pygame.Rect((self.snake.positions[0][0] * CELL_SIZE[0],
                                            self.snake.positions[0][1] * CELL_SIZE[1]), CELL_SIZE))
        if self.apple_pos:
            self.screen.blit(Assets.APPLE.surface,
                             pygame.Rect((self.apple_pos[0] * CELL_SIZE[0], self.apple_pos[1] * CELL_SIZE[1]), CELL_SIZE))
        font = pygame.font.SysFont(None, 28)
        if self.running:
            imgcolor = (192, 192, 192)
        else: 
            imgcolor = (255, 255, 0)
        text_prefix = ''
        if self.paused:
            text_prefix = '|| '
        elif len(self.snake.positions) == FIELD_SIZE[0] * FIELD_SIZE[1]:
            text_prefix = 'V '
        elif not self.running:
            text_prefix = 'X '
        img = font.render(f'{text_prefix}{len(self.snake.positions)}', True, imgcolor)
        self.screen.blit(img, (10, 10))
        pygame.display.flip()
    
    def dispatch_interactive_move(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
            elif event.type == pygame.KEYUP:
                if self.running:
                    if event.key == pygame.K_p:
                        self.running = False
                        self.paused = True
                    elif event.key in [pygame.K_UP, pygame.K_w] and self.snake.last_step_direction != Direction.DOWN:
                        self.snake.direction = Direction.UP
                    elif event.key in [pygame.K_a, pygame.K_LEFT] and self.snake.last_step_direction != Direction.RIGHT:
                        self.snake.direction = Direction.LEFT
                    elif event.key in [pygame.K_s, pygame.K_DOWN] and self.snake.last_step_direction != Direction.UP:
                        self.snake.direction = Direction.DOWN
                    elif event.key in [pygame.K_d, pygame.K_RIGHT] and self.snake.last_step_direction != Direction.LEFT:
                        self.snake.direction = Direction.RIGHT
                else:
                    if event.key == pygame.K_r:
                        self.__init__()
                    elif event.key == pygame.K_p and self.paused:
                        self.running = True
                        self.paused = False

    def run(self, move_dispatcher=None):
        while True:
            self.clock.tick(60)
            if move_dispatcher is not None:
                move_dispatcher(self)
            else:
                self.dispatch_interactive_move()
            self.tick()

            
