# snake

## Install dependencies
Usage of `poetry install` is recommended

## Run:
If you use poetry, you can launch virtual environment with `poetry shell`. Alternatively, use `poetry run python -m snake` outside of `poetry shell`.
If you use virtualenv, use `python -m snake` to run snake.

There are some command-line parameters, which can be seen with flag `--help`.
Examples:
- default field size and delay, add walls: `-w`
- field 4x5, delay 0.4s: `-d 0.4 -f 4,5`

Neither setting field width less than 3 nor field size equal to 3x1 is recommended =)


Hotkeys:
- `p` to pause and unpause
- `r` to restart after victory or loss
- `w/a/s/d or arrows` to change snake direction
